from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.after_request
def add_header(resp):
    resp.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    resp.headers["Pragma"] = "no-cache"
    resp.headers["Expires"] = "0"
    return resp

@app.route('/1')
def getForm1():
    return render_template('Form1.html')

@app.route('/2', methods=['POST'])
def getForm2():
    return render_template('Form2.html')

if __name__ == '__main__':
    app.run(debug=True)
